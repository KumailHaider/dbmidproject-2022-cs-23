﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Security.Cryptography;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;


namespace Jewellery_Store
{
    public partial class Form9 : Form
    {
        public void populate()
        {
            
        }
        public Form9()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            Form5 f5 = new Form5();
            f5.Show();
        }

        

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void Form9_Load(object sender, EventArgs e)
        {
            //populate();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                if (dateTimePicker1.Text == "")
                {
                    MessageBox.Show("Enter value in the field!!");
                }
                else
                {
                    try
                    {
                        // Convert the date to the expected SQL format
                        string dateOfCreated = dateTimePicker1.Value.ToString("yyyy-MM-dd");

                        // Insert the group data
                        string insertQuery = "INSERT INTO [Group] (Created_On) VALUES(@Created_On)";
                        SqlCommand cmd = new SqlCommand(insertQuery, con);
                        cmd.Parameters.AddWithValue("@Created_On", dateOfCreated);
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Group data entered Successfully!!");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: " + ex.Message);
                    }
                }
            }

        }
        private void LoadGroupData()
        {
            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();

                SqlCommand cmd = new SqlCommand("SELECT Id,Created_On FROM [Group]", con);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);

                dataGridView1.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
        private void button5_Click(object sender, EventArgs e)
        {
           LoadGroupData();
        }

        private void pictureBox4_Click_1(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Group Table!!.");
                return;
            }
            // Check if a row is selected in the DataGridView
            else if (dataGridView1.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int id = Convert.ToInt32(selectedRow.Cells["ID"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    // Delete record from Student table
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("DELETE FROM [Group] WHERE Id = @Id", con);
                        cmd.Parameters.AddWithValue("@Id", id);
                        cmd.ExecuteNonQuery();
                    }

                    MessageBox.Show("Record deleted successfully from the Group table!!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please select a row to delete!");
            }
        }
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Group Table.");
                return;
            }

            // Get the selected row
            DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];

            // Get the value of the ID column from the selected row
            int projectId = Convert.ToInt32(selectedRow.Cells["ID"].Value);

            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                // Retrieve data from Project table based on the selected ID
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM [Group] WHERE Id = @Id", con);
                    cmd.Parameters.AddWithValue("@Id", projectId);
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        // Fill the textboxes with the relevant project's records
                        dateTimePicker1.Text = reader["Created_On"].ToString();
                        
                    }
                    else
                    {
                        MessageBox.Show("No records found in Group table for the selected ID.");
                        return;
                    }

                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dataGridView1_SelectionChanged(sender, e);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            UpdateButton(sender, e);
        }
        private void UpdateButton(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Group Table.");
                return;
            }
            else if (dataGridView1.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int groupId = Convert.ToInt32(selectedRow.Cells["ID"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();

                        // Update record in Group table
                        SqlCommand updateGroupCmd = new SqlCommand("UPDATE [Group] SET Created_On=@Created_On WHERE Id = @Id", con);
                        updateGroupCmd.Parameters.AddWithValue("@Created_On", dateTimePicker1.Value); // Assuming dateTimePicker1 is the control for selecting the date
                        updateGroupCmd.Parameters.AddWithValue("@Id", groupId);
                        updateGroupCmd.ExecuteNonQuery();

                        MessageBox.Show("Record updated successfully in Group table!!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please select a row to update!");
            }
        }
        private void InsertRecord(object sender, EventArgs e)
        {
            // Validate if all required fields are filled
            if (regno.Text == "" || dateTimePicker2.Text == "" || comboBox1.Text == ""||dateTimePicker3.Text=="")
            {
                MessageBox.Show("Please fill all the required fields!");
                return;
            }

            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                // Establish connection to the database
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();

                    // Retrieve student ID based on registration number
                    int studentId;
                    SqlCommand getStudentIdCmd = new SqlCommand("SELECT Id FROM Student WHERE RegistrationNo = @RegistrationNo", con);
                    getStudentIdCmd.Parameters.AddWithValue("@RegistrationNo", regno.Text);
                    object studentIdResult = getStudentIdCmd.ExecuteScalar();

                    if (studentIdResult == null)
                    {
                        MessageBox.Show("No student found with the provided registration number.");
                        return;
                    }
                    else
                    {
                        studentId = Convert.ToInt32(studentIdResult);
                    }

                    // Convert the date to the expected SQL format
                    string assignmentDate = dateTimePicker3.Value.ToString("yyyy-MM-dd");
                    string groupcreationdate=dateTimePicker2.Value.ToString("yyyy-MM-dd");
                    // Get the status ID based on the selected status
                    int statusId;
                    switch (comboBox1.Text)
                    {
                        case "Active":
                            statusId = 3;
                            break;
                        case "InActive":
                            statusId = 4;
                            break;
                        default:
                            MessageBox.Show("Invalid status selected.");
                            return;
                    }

                    // Retrieve group ID based on group creation date
                    int groupId;
                    SqlCommand getGroupIdCmd = new SqlCommand("SELECT Id FROM [Group] WHERE Created_On = @CreatedOn", con);
                    getGroupIdCmd.Parameters.AddWithValue("@CreatedOn", groupcreationdate);
                    object groupIdResult = getGroupIdCmd.ExecuteScalar();

                    if (groupIdResult == null)
                    {
                        MessageBox.Show("No group found with the provided creation date.");
                        return;
                    }
                    else
                    {
                        groupId = Convert.ToInt32(groupIdResult);
                    }

                    // Check if the group already has 4 students
                    SqlCommand countStudentsCmd = new SqlCommand("SELECT COUNT(*) FROM GroupStudent WHERE GroupId = @GroupId", con);
                    countStudentsCmd.Parameters.AddWithValue("@GroupId", groupId);
                    int studentCount = (int)countStudentsCmd.ExecuteScalar();

                    if (studentCount >= 4)
                    {
                        MessageBox.Show("The limit of students for this group has been reached.");
                        return;
                    }

                    // Insert record into the table
                    SqlCommand insertCmd = new SqlCommand("INSERT INTO GroupStudent (StudentId, GroupId, Status, AssignmentDate) VALUES (@StudentId, @GroupId, @Status, @AssignmentDate)", con);
                    insertCmd.Parameters.AddWithValue("@StudentId", studentId);
                    insertCmd.Parameters.AddWithValue("@GroupId", groupId);
                    insertCmd.Parameters.AddWithValue("@Status", statusId);
                    insertCmd.Parameters.AddWithValue("@AssignmentDate", assignmentDate);
                    insertCmd.ExecuteNonQuery();

                    MessageBox.Show("Record Added successfully in Group Student Table!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            InsertRecord(sender, e);
        }
        private void LoadGroupStudentData()
        {
            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();

                SqlCommand cmd = new SqlCommand("SELECT GroupId,StudentId,Status,AssignmentDate FROM GroupStudent", con);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);

                dataGridView2.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            LoadGroupStudentData();
        }

        private void dataGridView2_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Group Student Table!!.");
                return;
            }
            else if (dataGridView2.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView2.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int groupId = Convert.ToInt32(selectedRow.Cells["GroupId"].Value);
                int studentId = Convert.ToInt32(selectedRow.Cells["StudentId"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";



                    // Retrieve data from Student table based on the selected ID
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE Id = @StudentId", con);
                        cmd.Parameters.AddWithValue("@StudentId", studentId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            // Fill the textboxes with the relevant project's records
                            regno.Text = reader["RegistrationNo"].ToString();

                            // Repeat this for other textboxes as needed
                        }
                        else
                        {
                            MessageBox.Show("No records found in Student table for the selected ID.");
                            return;
                        }

                        reader.Close();
                    }

                    // Retrieve data from Group table based on the selected ID
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("SELECT * FROM [Group] WHERE Id = @Id", con);
                        cmd.Parameters.AddWithValue("@Id", groupId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            // Fill the textboxes with the relevant group's records
                            dateTimePicker2.Text = reader["Created_On"].ToString();
                            //comboBox1.Text = reader["AdvisorRole"].ToString();

                        }
                        else
                        {
                            MessageBox.Show("No records found in Group table for the selected ID.");
                            return;
                        }

                        reader.Close();
                    }
                    // Retrieve data from GroupStudent table based on the selected ID
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("SELECT * FROM GroupStudent WHERE GroupId = @GroupId", con);
                        cmd.Parameters.AddWithValue("@GroupId", groupId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            // Fill the textboxes with the relevant group's records
                            dateTimePicker3.Text = reader["AssignmentDate"].ToString();
                            comboBox1.Text = reader["Status"].ToString();

                        }
                        else
                        {
                            MessageBox.Show("No records found in Group table for the selected ID.");
                            return;
                        }

                        reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }

        }

        private void button7_Click(object sender, EventArgs e)
        {
            dataGridView2_SelectionChanged(sender, e);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Group Student Table!!.");
                return;
            }
            // Check if a row is selected in the DataGridView
            else if (dataGridView2.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView2.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int id = Convert.ToInt32(selectedRow.Cells["StudentId"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    // Delete record from Student table
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("DELETE FROM GroupStudent WHERE StudentId = @StudentId", con);
                        cmd.Parameters.AddWithValue("@StudentId", id);
                        cmd.ExecuteNonQuery();
                    }

                    MessageBox.Show("Record deleted successfully from the Group Student table!!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please select a row to delete!");
            }
        }
        private void UpdateGroupStudentButton(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Group Student Table.");
                return;
            }
            else if (dataGridView2.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView2.SelectedRows[0];

                // Get the value of the Group ID and Student ID columns from the selected row
                int originalGroupId = Convert.ToInt32(selectedRow.Cells["GroupId"].Value);
                int originalStudentId = Convert.ToInt32(selectedRow.Cells["StudentId"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();

                        // Update record in Group Student table
                        SqlCommand updateGroupStudentCmd = new SqlCommand("UPDATE GroupStudent SET GroupId=@NewGroupId, StudentId=@NewStudentId, Status=@Status, AssignmentDate=@AssignmentDate WHERE GroupId = @OriginalGroupId AND StudentId = @OriginalStudentId", con);

                        // Assuming comboBox1 is the control for selecting status and dateTimePicker3 is the control for selecting the date
                        updateGroupStudentCmd.Parameters.AddWithValue("@NewGroupId", GetNewGroupId());
                        updateGroupStudentCmd.Parameters.AddWithValue("@NewStudentId", GetNewStudentId());
                        updateGroupStudentCmd.Parameters.AddWithValue("@Status", GetStatusId(comboBox1.Text));
                        updateGroupStudentCmd.Parameters.AddWithValue("@AssignmentDate", dateTimePicker3.Value);
                        updateGroupStudentCmd.Parameters.AddWithValue("@OriginalGroupId", originalGroupId);
                        updateGroupStudentCmd.Parameters.AddWithValue("@OriginalStudentId", originalStudentId);

                        updateGroupStudentCmd.ExecuteNonQuery();

                        MessageBox.Show("Record updated successfully in Group Student table!!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please select a row to update!");
            }

        }

        // Helper function to get the status ID based on the status string
        private int GetStatusId(string status)
        {
            switch (status)
            {
                case "Active":
                    return 3;
                case "InActive":
                    return 4;
                default:
                    throw new ArgumentException("Invalid status: " + status);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            UpdateGroupStudentButton(sender, e);
        }

        // Method to get a new Group ID based on creation date from the DateTimePicker
        private int GetNewGroupId()
        {
            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();

                    // Retrieve the Group ID based on the creation date
                    DateTime creationDate = dateTimePicker2.Value;
                    SqlCommand cmd = new SqlCommand("SELECT Id FROM [Group] WHERE Created_On = @Created_On", con);
                    cmd.Parameters.AddWithValue("@Created_On", creationDate);

                    object groupIdResult = cmd.ExecuteScalar();
                    if (groupIdResult != null)
                    {
                        return Convert.ToInt32(groupIdResult);
                    }
                    else
                    {
                        MessageBox.Show("No group found for the specified creation date.");
                        // You may want to handle this case based on your application logic
                        return -1; // or throw an exception, depending on your requirement
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                // You may want to handle this exception based on your application logic
                return -1; // or throw an exception, depending on your requirement
            }
        }

        // Method to get a new Student ID based on the registration number
        private int GetNewStudentId()
        {
            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();

                    // Retrieve the Student ID based on the registration number
                    string registrationNumber = regno.Text; // Assuming registrationNoTextBox is the TextBox for registration number
                    SqlCommand cmd = new SqlCommand("SELECT Id FROM Student WHERE RegistrationNo = @RegistrationNo", con);
                    cmd.Parameters.AddWithValue("@RegistrationNo", registrationNumber);

                    object studentIdResult = cmd.ExecuteScalar();
                    if (studentIdResult != null)
                    {
                        return Convert.ToInt32(studentIdResult);
                    }
                    else
                    {
                        MessageBox.Show("No student found for the specified registration number.");
                        // You may want to handle this case based on your application logic
                        return -1; // or throw an exception, depending on your requirement
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                // You may want to handle this exception based on your application logic
                return -1; // or throw an exception, depending on your requirement
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Close();
            Form2 f2 = new Form2();
            f2.Show();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
            Form2 f2 = new Form2();
            f2.Show();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            this.Close();
            Form3 f3 = new Form3();
            f3.Show();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
            Form3 f3 = new Form3();
            f3.Show();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Close();
            Form4 f4 = new Form4();
            f4.Show();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            this.Close();
            Form4 f4 = new Form4();
            f4.Show();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            this.Close();
            Form5 f5 = new Form5();
            f5.Show();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Close();
            Form5 f5 = new Form5();
            f5.Show();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            this.Close();
            Form9 f9 = new Form9();
            f9.Show();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Close();
            Form9 f9 = new Form9();
            f9.Show();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            this.Close();
            Form10 f10 = new Form10();
            f10.Show();
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            this.Close();
            Form10 f10 = new Form10();
            f10.Show();
        }

        private void label16_Click(object sender, EventArgs e)
        {
            this.Close();
            Form6 f6 = new Form6();
            f6.Show();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            this.Close();
            Form6 f6 = new Form6();
            f6.Show();
        }
    }
}
