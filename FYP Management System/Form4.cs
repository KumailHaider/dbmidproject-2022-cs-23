﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace Jewellery_Store
{ 
    public partial class Form4 : Form
    {
        
        public Form4()
        {
            InitializeComponent();
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
          
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                if (eid.Text == "" || ename.Text == "" || ecnic.Text == "" || eph.Text == "" || comboBox1.Text == ""||comboBox2.Text==""||cid.Text=="")
                {
                    MessageBox.Show("Please fill all the fields");
                }
                else
                {
                    try
                    {
                        // Convert the date to the expected SQL format
                        string dateOfBirth = dateTimePicker1.Value.ToString("yyyy-MM-dd");

                        // Check if a person with the same details already exists
                        SqlCommand checkCmd = new SqlCommand("SELECT Id FROM Person WHERE FirstName = @FirstName AND LastName = @LastName AND Contact = @Contact AND Email = @Email AND DateOfBirth = @DateOfBirth AND Gender = @Gender", con);
                        checkCmd.Parameters.AddWithValue("@FirstName", eid.Text);
                        checkCmd.Parameters.AddWithValue("@LastName", ename.Text);
                        checkCmd.Parameters.AddWithValue("@Contact", ecnic.Text);
                        checkCmd.Parameters.AddWithValue("@Email", eph.Text);
                        checkCmd.Parameters.AddWithValue("@DateOfBirth", dateOfBirth);
                        if (comboBox2.Text == "Male")
                        {
                            checkCmd.Parameters.AddWithValue("@Gender", 1);
                        }
                        else
                        {
                            checkCmd.Parameters.AddWithValue("@Gender", 2);
                        }

                        object personIdResult = checkCmd.ExecuteScalar();

                        if (personIdResult != null)
                        {
                            int personId = Convert.ToInt32(personIdResult);

                            // Insert record into Advisor table using the existing person ID
                            SqlCommand insertAdvisorCmd = new SqlCommand("INSERT INTO Advisor (Id, Designation,Salary) VALUES (@Id, @Designation,@Salary)", con);
                            insertAdvisorCmd.Parameters.AddWithValue("@Id", personId);
                            insertAdvisorCmd.Parameters.AddWithValue("@Salary", cid.Text);
                            if (comboBox1.Text == "Professor")
                            {
                                insertAdvisorCmd.Parameters.AddWithValue("@Designation", 6);
                            }
                            else if (comboBox1.Text == "AssociateProfessor")
                            {
                                insertAdvisorCmd.Parameters.AddWithValue("@Designation", 7);
                            }
                            else if (comboBox1.Text == "AssisstantProfessor")
                            {
                                insertAdvisorCmd.Parameters.AddWithValue("@Designation", 8);
                            }
                            else if (comboBox1.Text == "Lecturer")
                            {
                                insertAdvisorCmd.Parameters.AddWithValue("@Designation", 9);
                            }
                            else
                            {
                                insertAdvisorCmd.Parameters.AddWithValue("@Designation", 10);
                            }
                            insertAdvisorCmd.ExecuteNonQuery();

                            MessageBox.Show("Record Added Successfully using existing person details!");
                        }
                        else
                        {
                            // If person with the same details doesn't exist, insert a new person first
                            SqlCommand insertPersonCmd = new SqlCommand("INSERT INTO Person (FirstName, LastName, Contact, Email, DateOfBirth, Gender) VALUES (@FirstName, @LastName, @Contact, @Email, @DateOfBirth, @Gender); SELECT SCOPE_IDENTITY()", con);

                            insertPersonCmd.Parameters.AddWithValue("@FirstName", eid.Text);
                            insertPersonCmd.Parameters.AddWithValue("@LastName", ename.Text);
                            insertPersonCmd.Parameters.AddWithValue("@Contact", ecnic.Text);
                            insertPersonCmd.Parameters.AddWithValue("@Email", eph.Text);
                            insertPersonCmd.Parameters.AddWithValue("@DateOfBirth", dateOfBirth);
                            if (comboBox2.Text == "Male")
                            {
                                insertPersonCmd.Parameters.AddWithValue("@Gender", 1);
                            }
                            else
                            {
                                insertPersonCmd.Parameters.AddWithValue("@Gender", 2);
                            }

                            // Execute the command to insert the person and retrieve the generated ID
                            int newPersonId = Convert.ToInt32(insertPersonCmd.ExecuteScalar());

                            // Insert record into Advisor table using the newly inserted person ID
                            SqlCommand insertAdvisorCmd = new SqlCommand("INSERT INTO Advisor (Id, Designation,Salary) VALUES (@Id, @Designation,@Salary)", con);
                            insertAdvisorCmd.Parameters.AddWithValue("@Id", newPersonId);
                            insertAdvisorCmd.Parameters.AddWithValue("@Salary", cid.Text);
                            if (comboBox1.Text == "Professor")
                            {
                                insertAdvisorCmd.Parameters.AddWithValue("@Designation", 6);
                            }
                            else if (comboBox1.Text == "AssociateProfessor")
                            {
                                insertAdvisorCmd.Parameters.AddWithValue("@Designation", 7);
                            }
                            else if (comboBox1.Text == "AssisstantProfessor")
                            {
                                insertAdvisorCmd.Parameters.AddWithValue("@Designation", 8);
                            }
                            else if (comboBox1.Text == "Lecturer")
                            {
                                insertAdvisorCmd.Parameters.AddWithValue("@Designation", 9);
                            }
                            else
                            {
                                insertAdvisorCmd.Parameters.AddWithValue("@Designation", 10);
                            }
                            insertAdvisorCmd.ExecuteNonQuery();


                            MessageBox.Show("Record Added Successfully using new person details!");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: " + ex.Message);
                    }
                }
            }


        }
        private void LoadAdvisorData()
        {
            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();

                SqlCommand cmd = new SqlCommand("SELECT Id, Designation,Salary FROM Advisor", con);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);

                dataGridView1.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            UpdateButton_Click(sender, e);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dataGridView1_SelectionChanged(sender, e);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Advisor Table!!.");
                return;
            }
            // Check if a row is selected in the DataGridView
            else if (dataGridView1.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int id = Convert.ToInt32(selectedRow.Cells["ID"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    // Delete record from Student table
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("DELETE FROM Advisor WHERE Id = @Id", con);
                        cmd.Parameters.AddWithValue("@Id", id);
                        cmd.ExecuteNonQuery();
                    }

                    MessageBox.Show("Record deleted successfully from the Advisor table!!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please select a row to delete!");
            }

        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 frm2 = new Form2();
            frm2.Show();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 frm2 = new Form2();
            frm2.Show();
        }

        private void label3_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form3 f3 = new Form3();
            f3.Show();
        }


        private void pictureBox4_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            
            LoadAdvisorData();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            this.Close();
            Form6 f6 = new Form6();
            f6.Show();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Close();
            Form6 f6 = new Form6();
            f6.Show();
        }
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Advisor Table!!.");
                return;
            }
            else if (dataGridView1.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int id = Convert.ToInt32(selectedRow.Cells["ID"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    // Retrieve data from Person table based on the selected ID
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("SELECT * FROM Person WHERE Id = @Id", con);
                        cmd.Parameters.AddWithValue("@Id", id);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            // Fill the textboxes with the relevant person's records
                            eid.Text = reader["FirstName"].ToString();
                            ename.Text = reader["LastName"].ToString();
                            ecnic.Text = reader["Contact"].ToString();
                            eph.Text = reader["Email"].ToString();
                            comboBox2.Text = reader["Gender"].ToString();

                            // Repeat this for other textboxes as needed
                        }
                        else
                        {
                            MessageBox.Show("No records found in Person table for the selected ID.");
                            return;
                        }

                        reader.Close();
                    }
                    // Retrieve data from Advisor table based on the selected ID
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("SELECT * FROM Advisor WHERE Id = @Id", con);
                        cmd.Parameters.AddWithValue("@Id", id);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            // Fill the textboxes with the relevant advisor's records
                            cid.Text = reader["Salary"].ToString();
                            comboBox1.Text = reader["Designation"].ToString() ;

                        }
                        else
                        {
                            MessageBox.Show("No records found in Advisor table for the selected ID.");
                            return;
                        }

                        reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
        }
        private void UpdateButton_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Advisor Table!!.");
                return;
            }
            else if (dataGridView1.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int id = Convert.ToInt32(selectedRow.Cells["ID"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();

                        // Update record in Advisor table
                        SqlCommand updateAdvisorCmd = new SqlCommand("UPDATE Advisor SET Designation=@Designation, Salary=@Salary WHERE Id = @Id", con);
                        updateAdvisorCmd.Parameters.AddWithValue("@Designation", GetDesignationId(comboBox1.Text));
                        updateAdvisorCmd.Parameters.AddWithValue("@Salary", cid.Text);
                        updateAdvisorCmd.Parameters.AddWithValue("@Id", id);
                        updateAdvisorCmd.ExecuteNonQuery();

                        // Update record in Person table
                        SqlCommand updatePersonCmd = new SqlCommand("UPDATE Person SET FirstName = @FirstName, LastName = @LastName, Contact=@Contact, Email=@Email WHERE Id = @Id", con);
                        updatePersonCmd.Parameters.AddWithValue("@FirstName", eid.Text);
                        updatePersonCmd.Parameters.AddWithValue("@LastName", ename.Text);
                        updatePersonCmd.Parameters.AddWithValue("@Contact", ecnic.Text);
                        updatePersonCmd.Parameters.AddWithValue("@Email", eph.Text);
                        updatePersonCmd.Parameters.AddWithValue("@Id", id);
                        updatePersonCmd.ExecuteNonQuery();

                        MessageBox.Show("Record updated successfully in both Advisor and Person tables!!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please select a row to update!");
            }
        }
        private int GetDesignationId(string designation)
        {
            // Define a dictionary to map designation strings to their corresponding integer values
            Dictionary<string, int> designationMappings = new Dictionary<string, int>
            {
                { "Professor", 6 },
                { "AssociateProfessor", 7 },
                {"AssistantProfessor", 8 },
                { "Lecturer", 9 },
                {"IndustryProfessional",10 },
        // Add more mappings as needed
            };

            // Get the integer designation based on the selected string value
            return designationMappings.ContainsKey(designation) ? designationMappings[designation] : 10;
        }

        private void pictureBox2_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form3 f3 = new Form3();
            f3.Show();
        }

        private void label4_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form5 f5 = new Form5();
            f5.Show();
        }

        private void pictureBox3_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form5 f5 = new Form5();
            f5.Show();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            this.Close();
            Form f9 = new Form9();
            f9.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
            Form8 f8 = new Form8();
            f8.Show();
        }

        private void label2_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form2 f2 = new Form2();
            f2.Show();
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form2 f2 = new Form2();
            f2.Show();
        }

        private void pictureBox6_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form f9 = new Form9();
            f9.Show();
        }

        private void label6_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form10 f10= new Form10();
            f10.Show();
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            this.Close();
            Form10 f10 = new Form10();
            f10.Show();
        }

        private void label16_Click(object sender, EventArgs e)
        {
            this.Close();
            Form6 f6= new Form6();
            f6.Show();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            this.Close();
            Form6 f6 = new Form6();
            f6.Show();
        }
    }
}
