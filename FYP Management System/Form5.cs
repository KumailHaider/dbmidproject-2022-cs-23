﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using System.Text.RegularExpressions;

namespace Jewellery_Store
{
    public partial class Form5 : Form
    {
        
        public Form5()
        {
            InitializeComponent();
        }

        private void Form5_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Project Table!!.");
                return;
            }
            // Check if a row is selected in the DataGridView
            else if (dataGridView1.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int id = Convert.ToInt32(selectedRow.Cells["ID"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    // Delete record from Student table
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("DELETE FROM Project WHERE Id = @Id", con);
                        cmd.Parameters.AddWithValue("@Id", id);
                        cmd.ExecuteNonQuery();
                    }

                    MessageBox.Show("Record deleted successfully from the Project table!!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please select a row to delete!");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                if (pid.Text == "" || pdes.Text == "")
                {
                    MessageBox.Show("Enter values in all fields!!");
                }
                else
                {
                    try
                    {
                        // Check if the project with the same title already exists
                        string checkQuery = "SELECT COUNT(*) FROM Project WHERE Title = @Title";
                        SqlCommand checkCmd = new SqlCommand(checkQuery, con);
                        checkCmd.Parameters.AddWithValue("@Title", pid.Text);
                        int existingCount = (int)checkCmd.ExecuteScalar();

                        if (existingCount > 0)
                        {
                            MessageBox.Show("Project with the same title already exists!");
                        }
                        else
                        {
                            // Insert the project data if it doesn't already exist
                            string insertQuery = "INSERT INTO Project(Title, Description) VALUES(@Title, @Description)";
                            SqlCommand cmd = new SqlCommand(insertQuery, con);
                            cmd.Parameters.AddWithValue("@Title", pid.Text);
                            cmd.Parameters.AddWithValue("@Description", pdes.Text);
                            cmd.ExecuteNonQuery();
                            MessageBox.Show("Project data entered Successfully!!");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: " + ex.Message);
                    }
                }
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            UpdateButton(sender, e);
        }

       

        

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            
            LoadProjectData();
        }
        private void LoadProjectData()
        {
            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();

                SqlCommand cmd = new SqlCommand("SELECT Id, Description,Title FROM Project", con);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);

                dataGridView1.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
        private void label6_Click(object sender, EventArgs e)
        {
            this.Close();
            Form6 f6 = new Form6();
            f6.Show();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Close();
            Form6 f6 = new Form6();
            f6.Show();
        }

        

        private void button3_Click(object sender, EventArgs e)
        {
            
            dataGridView1_SelectionChanged(sender, e);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Project Table.");
                return;
            }

            // Get the selected row
            DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];

            // Get the value of the ID column from the selected row
            int projectId = Convert.ToInt32(selectedRow.Cells["ID"].Value);

            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                // Retrieve data from Project table based on the selected ID
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM Project WHERE Id = @Id", con);
                    cmd.Parameters.AddWithValue("@Id", projectId);
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        // Fill the textboxes with the relevant project's records
                        pid.Text = reader["Title"].ToString();
                        pdes.Text = reader["Description"].ToString();
                    }
                    else
                    {
                        MessageBox.Show("No records found in Project table for the selected ID.");
                        return;
                    }

                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
        private void UpdateButton(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Projects Table.");
                return;
            }
            else if (dataGridView1.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int projectId = Convert.ToInt32(selectedRow.Cells["ID"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();

                        // Update record in Projects table
                        SqlCommand updateProjectCmd = new SqlCommand("UPDATE Project SET Title=@Title, Description=@Description WHERE Id = @Id", con);
                        updateProjectCmd.Parameters.AddWithValue("@Title", pid.Text);
                        updateProjectCmd.Parameters.AddWithValue("@Description", pdes.Text);
                        updateProjectCmd.Parameters.AddWithValue("@Id", projectId);
                        updateProjectCmd.ExecuteNonQuery();

                        MessageBox.Show("Record updated successfully in Project table!!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please select a row to update!");
            }

        }

        private void label2_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            Form2 frm2 = new Form2();
            frm2.Show();
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            Form2 frm2 = new Form2();
            frm2.Show();
        }

        private void label3_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form3 f3 = new Form3();
            f3.Show();
        }

        private void pictureBox2_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form3 f3 = new Form3();
            f3.Show();
        }

        private void label5_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form4 f4 = new Form4();
            f4.Show();
        }

        private void pictureBox5_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form4 f4 = new Form4();
            f4.Show();
        }

        private void label4_Click_1(object sender, EventArgs e)
        {
            this.Refresh();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Refresh();
            
        }

        private void label17_Click(object sender, EventArgs e)
        {
            this.Close();
            Form9 f9 =new Form9();
            f9.Show();
        }

        private void pictureBox6_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form9 f9 = new Form9();
            f9.Show();
        }

        private void label6_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form10 f10=new Form10();
            f10.Show();
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            this.Close();
            Form10 f10 = new Form10();
            f10.Show();
        }

        private void label16_Click(object sender, EventArgs e)
        {
            this.Close();
            Form6 f6=new Form6();
            f6.Show();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            this.Close();
            Form6 f6 = new Form6();
            f6.Show();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            LoadGroupProjectData();
        }
        private void InsertRecord()
        {
            if (ptitle.Text == "" || dateTimePicker2.Text == "" || dateTimePicker3.Text == "")
            {
                MessageBox.Show("Enter values in all fields!!");
            }
            else
            {
                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();

                        // Retrieve the Project ID based on the title
                        string projectTitle = ptitle.Text;
                        SqlCommand projectCmd = new SqlCommand("SELECT Id FROM Project WHERE Title = @Title", con);
                        projectCmd.Parameters.AddWithValue("@Title", projectTitle);

                        object projectIdResult = projectCmd.ExecuteScalar();
                        if (projectIdResult == null)
                        {
                            MessageBox.Show("No project found for the specified title.");
                            return;
                        }
                        int projectId = Convert.ToInt32(projectIdResult);

                        // Retrieve the Group ID based on the creation date
                        DateTime creationDate = dateTimePicker2.Value.Date; // Use Date property to ignore time component
                        SqlCommand groupCmd = new SqlCommand("SELECT Id FROM [Group] WHERE CONVERT(date, Created_On) = @Created_On", con);
                        groupCmd.Parameters.AddWithValue("@Created_On", creationDate);

                        object groupIdResult = groupCmd.ExecuteScalar();
                        if (groupIdResult == null)
                        {
                            MessageBox.Show("No group found for the specified creation date.");
                            return;
                        }
                        int groupId = Convert.ToInt32(groupIdResult);

                        // Get the assignment date from the DateTimePicker
                        DateTime assignmentDate = dateTimePicker3.Value;

                        // Insert record into the GroupProject table
                        SqlCommand insertCmd = new SqlCommand("INSERT INTO GroupProject (ProjectId, GroupId, AssignmentDate) VALUES (@ProjectId, @GroupId, @AssignmentDate)", con);
                        insertCmd.Parameters.AddWithValue("@ProjectId", projectId);
                        insertCmd.Parameters.AddWithValue("@GroupId", groupId);
                        insertCmd.Parameters.AddWithValue("@AssignmentDate", assignmentDate);
                        insertCmd.ExecuteNonQuery();

                        MessageBox.Show("Record Added successfully in Group Project table!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
        }


        private void button9_Click(object sender, EventArgs e)
        {
            InsertRecord();
        }
        private void LoadGroupProjectData()
        {
            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();

                SqlCommand cmd = new SqlCommand("SELECT GroupId, ProjectId,AssignmentDate FROM GroupProject", con);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);

                dataGridView2.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
        private void dataGridView2_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Group Project Table!!.");
                return;
            }
            else if (dataGridView2.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView2.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int groupId = Convert.ToInt32(selectedRow.Cells["GroupId"].Value);
                int projectId = Convert.ToInt32(selectedRow.Cells["ProjectId"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";



                    // Retrieve data from Project table based on the selected ID
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("SELECT * FROM Project WHERE Id = @ProjectId", con);
                        cmd.Parameters.AddWithValue("@ProjectId", projectId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            // Fill the textboxes with the relevant project's records
                            ptitle.Text = reader["Title"].ToString();

                            // Repeat this for other textboxes as needed
                        }
                        else
                        {
                            MessageBox.Show("No records found in Project table for the selected ID.");
                            return;
                        }

                        reader.Close();
                    }

                    // Retrieve data from Group table based on the selected ID
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("SELECT * FROM [Group] WHERE Id = @Id", con);
                        cmd.Parameters.AddWithValue("@Id", groupId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            // Fill the textboxes with the relevant group's records
                            dateTimePicker2.Text = reader["Created_On"].ToString();
                            //comboBox1.Text = reader["AdvisorRole"].ToString();

                        }
                        else
                        {
                            MessageBox.Show("No records found in Group table for the selected ID.");
                            return;
                        }

                        reader.Close();
                    }
                    // Retrieve data from GroupProject table based on the selected ID
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("SELECT * FROM GroupProject WHERE GroupId = @GroupId", con);
                        cmd.Parameters.AddWithValue("@GroupId", groupId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            // Fill the textboxes with the relevant group's records
                            dateTimePicker3.Text = reader["AssignmentDate"].ToString();
                            

                        }
                        else
                        {
                            MessageBox.Show("No records found in Group Project table for the selected ID.");
                            return;
                        }

                        reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }

        }

        private void button7_Click(object sender, EventArgs e)
        {
            dataGridView2_SelectionChanged(sender, e);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Group Student Table!!.");
                return;
            }
            // Check if a row is selected in the DataGridView
            else if (dataGridView2.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView2.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int projectId = Convert.ToInt32(selectedRow.Cells["ProjectId"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    // Delete record from GroupStudent table
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("DELETE FROM GroupProject WHERE ProjectId = @ProjectId", con);
                        cmd.Parameters.AddWithValue("@ProjectId", projectId);
                        cmd.ExecuteNonQuery();
                    }

                    MessageBox.Show("Record deleted successfully from the Group Project table!!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please select a row to delete!");
            }

        }
        private void UpdateGroupProjectButton(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Group Project Table.");
                return;
            }
            else if (dataGridView2.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView2.SelectedRows[0];

                // Get the value of the Project ID and Group ID columns from the selected row
                int originalProjectId = Convert.ToInt32(selectedRow.Cells["ProjectId"].Value);
                int originalGroupId = Convert.ToInt32(selectedRow.Cells["GroupId"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();

                        // Update record in Group Project table
                        SqlCommand updateGroupProjectCmd = new SqlCommand("UPDATE GroupProject SET ProjectId=@NewProjectId, GroupId=@NewGroupId, AssignmentDate=@AssignmentDate WHERE ProjectId = @OriginalProjectId AND GroupId = @OriginalGroupId", con);

                        // Assuming dateTimePicker3 is the control for selecting the date
                        updateGroupProjectCmd.Parameters.AddWithValue("@NewProjectId", GetNewProjectId());
                        updateGroupProjectCmd.Parameters.AddWithValue("@NewGroupId", GetNewGroupId());
                        updateGroupProjectCmd.Parameters.AddWithValue("@AssignmentDate", dateTimePicker3.Value);
                        updateGroupProjectCmd.Parameters.AddWithValue("@OriginalProjectId", originalProjectId);
                        updateGroupProjectCmd.Parameters.AddWithValue("@OriginalGroupId", originalGroupId);

                        updateGroupProjectCmd.ExecuteNonQuery();

                        MessageBox.Show("Record updated successfully in Group Project table!!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please select a row to update!");
            }
        }

        private int GetNewGroupId()
        {
            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();

                    // Retrieve the Group ID based on the creation date
                    DateTime creationDate = dateTimePicker2.Value;
                    SqlCommand cmd = new SqlCommand("SELECT Id FROM [Group] WHERE Created_On = @Created_On", con);
                    cmd.Parameters.AddWithValue("@Created_On", creationDate);

                    object groupIdResult = cmd.ExecuteScalar();
                    if (groupIdResult != null)
                    {
                        return Convert.ToInt32(groupIdResult);
                    }
                    else
                    {
                        MessageBox.Show("No group found for the specified creation date.");
                        // You may want to handle this case based on your application logic
                        return -1; // or throw an exception, depending on your requirement
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                // You may want to handle this exception based on your application logic
                return -1; // or throw an exception, depending on your requirement
            }
        }

        // Method to get a new Project ID based on the project title
        private int GetNewProjectId()
        {
            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();

                    // Retrieve the Project ID based on the title
                    string projectTitle = ptitle.Text; // Assuming ptitle is the TextBox for project title
                    SqlCommand cmd = new SqlCommand("SELECT Id FROM Project WHERE Title = @Title", con);
                    cmd.Parameters.AddWithValue("@Title", projectTitle);

                    object projectIdResult = cmd.ExecuteScalar();
                    if (projectIdResult != null)
                    {
                        return Convert.ToInt32(projectIdResult);
                    }
                    else
                    {
                        MessageBox.Show("No project found for the specified title.");
                        // You may want to handle this case based on your application logic
                        return -1; // or throw an exception, depending on your requirement
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                // You may want to handle this exception based on your application logic
                return -1; // or throw an exception, depending on your requirement
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            UpdateGroupProjectButton(sender, e);
        }
    }
}
