﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Xml.Linq;
using System.IO;
//using iText.Kernel.Pdf;
//using iText.Layout;
//using iText.Layout.Element;





namespace Jewellery_Store
{
    public partial class Form10 : Form
    {
        public void populate()
        {
            
        }
        public Form10()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }


        private void pictureBox4_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void Form10_Load(object sender, EventArgs e)
        {
           
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Close();
            Form2 f2 = new Form2();
            f2.Show();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
            Form2 f2 = new Form2();
            f2.Show();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            this.Close();
            Form3 f3 = new Form3();
            f3.Show();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
            Form3 f3 = new Form3();
            f3.Show();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Close();
            Form4 f4 = new Form4();
            f4.Show();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            this.Close();
            Form4 f4 = new Form4();
            f4.Show();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            this.Close();
            Form5 f5 = new Form5();
            f5.Show();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Close();
            Form5 f5 = new Form5();
            f5.Show();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            this.Close();
            Form9 f9 = new Form9();
            f9.Show();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Close();
            Form9 f9 = new Form9();
            f9.Show();
        }

        private void label8_Click(object sender, EventArgs e)
        {
            this.Close();
            Form6 f6 = new Form6();
            f6.Show();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            this.Close();
            Form6 f6 = new Form6();
            f6.Show();
        }

        //public void GeneratePDFReport()
        //{
        //    // Create a new PDF document
        //    PdfWriter writer = new PdfWriter("report.pdf");
        //    PdfDocument pdf = new PdfDocument(writer);
        //    Document document = new Document(pdf);

        //    // Add title to the document
        //    Paragraph title = new Paragraph("List of Projects along with Advisory Board and List of Students");
        //    title.SetFontSize(16);
        //    title.SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
        //    document.Add(title);

        //    try
        //    {
        //        string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

        //        // Retrieve project data from the database
        //        using (SqlConnection connection = new SqlConnection(connectionString))
        //        {
        //            connection.Open();

        //            string projectQuery = "SELECT * FROM Project";
        //            SqlCommand projectCommand = new SqlCommand(projectQuery, connection);

        //            SqlDataReader projectReader = projectCommand.ExecuteReader();

        //            while (projectReader.Read())
        //            {
        //                string projectName = projectReader["Title"].ToString();
        //                int projectId = Convert.ToInt32(projectReader["ID"]);

        //                // Add project name
        //                Paragraph projectTitle = new Paragraph(projectName);
        //                projectTitle.SetBold();
        //                document.Add(projectTitle);

        //                // Add advisory board
        //                string advisoryBoardQuery = "SELECT Id FROM Advisor WHERE ID = @ID";
        //                SqlCommand advisoryBoardCommand = new SqlCommand(advisoryBoardQuery, connection);
        //                advisoryBoardCommand.Parameters.AddWithValue("@ID", projectId);

        //                SqlDataReader advisoryBoardReader = advisoryBoardCommand.ExecuteReader();
        //                if (advisoryBoardReader.Read())
        //                {
        //                    string advisoryBoard = advisoryBoardReader["Advisor"].ToString();
        //                    Paragraph advisoryBoardParagraph = new Paragraph("Advisor: " + advisoryBoard);
        //                    document.Add(advisoryBoardParagraph);
        //                }
        //                advisoryBoardReader.Close();

        //                // Add list of students
        //                Paragraph studentsParagraph = new Paragraph("List of Students:");
        //                document.Add(studentsParagraph);

        //                string studentsQuery = "SELECT FirstName,LastName FROM Student WHERE ID = @ID";
        //                SqlCommand studentsCommand = new SqlCommand(studentsQuery, connection);
        //                studentsCommand.Parameters.AddWithValue("@ID", projectId);

        //                SqlDataReader studentsReader = studentsCommand.ExecuteReader();
        //                while (studentsReader.Read())
        //                {
        //                    string studentName = studentsReader["FirstName,LastName"].ToString();
        //                    Paragraph studentParagraph = new Paragraph(studentName);
        //                    document.Add(studentParagraph);
        //                }
        //                studentsReader.Close();

        //                // Add a separator between projects
        //                //document.Add(new AreaBreak());
        //            }
        //            projectReader.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        // Handle exceptions
        //        Console.WriteLine("Error: " + ex.Message);
        //    }

        //    // Close the document
        //    document.Close();
        //}

        //private void button5_Click(object sender, EventArgs e)
        //{
        //    GeneratePDFReport();
        //}

        private DataTable GetDataTableFromDatabase()
        {
            DataTable dataTable = new DataTable();
            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";
                string query = "SELECT Project.Title, Advisor.Id AS AdvisorId, Student.RegistrationNo FROM Project CROSS JOIN Advisor CROSS JOIN Student;\r\n;\r\n"; // Replace with your actual SQL query

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(query, connection);
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dataTable);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            return dataTable;
        }
        void ExportDataTableToPdf(DataTable dtblTable, string strPdfPath, string strHeader)
        {
            using (FileStream fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                Document document = new Document(PageSize.A4);
                PdfWriter writer = PdfWriter.GetInstance(document, fs);
                document.Open();

                //Report Header
                iTextSharp.text.Font fontHeader = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 16, BaseColor.GRAY);
                Paragraph prgHeading = new Paragraph(strHeader.ToUpper(), fontHeader);
                prgHeading.Alignment = Element.ALIGN_CENTER;
                document.Add(prgHeading);

                //Author
                iTextSharp.text.Font fontAuthor = FontFactory.GetFont(FontFactory.HELVETICA, 8, BaseColor.GRAY);
                Paragraph prgAuthor = new Paragraph("Author : Kumail Haider\nRun Date : " + DateTime.Now.ToShortDateString(), fontAuthor);
                prgAuthor.Alignment = Element.ALIGN_RIGHT;
                document.Add(prgAuthor);

                //Add a line separation
                document.Add(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));

                //Write the table
                PdfPTable table = new PdfPTable(dtblTable.Columns.Count);
                //Table header
                iTextSharp.text.Font fontColumnHeader = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 10, BaseColor.WHITE);
                for (int i = 0; i < dtblTable.Columns.Count; i++)
                {
                    PdfPCell cell = new PdfPCell();
                    cell.BackgroundColor = BaseColor.GRAY;
                    cell.AddElement(new Chunk(dtblTable.Columns[i].ColumnName.ToUpper(), fontColumnHeader));
                    table.AddCell(cell);
                }
                //table Data
                for (int i = 0; i < dtblTable.Rows.Count; i++)
                {
                    for (int j = 0; j < dtblTable.Columns.Count; j++)
                    {
                        table.AddCell(dtblTable.Rows[i][j].ToString());
                    }
                }

                document.Add(table);
                document.Close();
                writer.Close();
            }
        }
        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtbl = GetDataTableFromDatabase();
                ExportDataTableToPdf(dtbl, @"C:\Users\JUNCTION\Documents\OneNote Notebooks\Jewellery-Store-Management-System-main\Report1.pdf", "List of projects along with advisory board and list of students ");
                if (checkBox1.Checked)
                {
                    System.Diagnostics.Process.Start(@"C:\Users\JUNCTION\Documents\OneNote Notebooks\Jewellery-Store-Management-System-main\Report1.pdf");
                    this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Message");
            }
        }

        //.....................................2..........................................

        private DataTable GetDataTableFromDatabase2()
        {
            DataTable dataTable = new DataTable();
            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";
                string query = "SELECT Student.RegistrationNo, Project.Title AS ProjectTitle, Evaluation.Name AS EvaluationName, GroupEvaluation.ObtainedMarks AS Marks FROM Student JOIN GroupStudent ON Student.Id = GroupStudent.StudentId JOIN [Group] ON GroupStudent.GroupId = [Group].Id JOIN GroupProject ON [Group].Id = GroupProject.GroupId JOIN Project ON GroupProject.ProjectId = Project.Id JOIN GroupEvaluation ON [Group].Id = GroupEvaluation.GroupId JOIN Evaluation ON GroupEvaluation.EvaluationId = Evaluation.Id;\r\n;\r\n;\r\n"; // Replace with your actual SQL query

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(query, connection);
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dataTable);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            return dataTable;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtbl = GetDataTableFromDatabase2();
                ExportDataTableToPdf(dtbl, @"C:\Users\JUNCTION\Documents\OneNote Notebooks\Jewellery-Store-Management-System-main\Report2.pdf", "Marks sheet of projects that shows the marks in each evaluation against each student and project ");
                if (checkBox2.Checked)
                {
                    System.Diagnostics.Process.Start(@"C:\Users\JUNCTION\Documents\OneNote Notebooks\Jewellery-Store-Management-System-main\Report2.pdf");
                    this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Message");
            }
        }

        //.....................................3..........................................

        private DataTable GetDataTableFromDatabase3()
        {
            DataTable dataTable = new DataTable();
            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";
                string query = "SELECT  GroupProject.* FROM GroupProject INNER JOIN GroupEvaluation ON GroupProject.ProjectId = GroupEvaluation.GroupId WHERE GroupEvaluation.ObtainedMarks < 15;\r\n"; // Replace with your actual SQL query

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(query, connection);
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dataTable);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            return dataTable;
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            try
            {
                DataTable dtbl = GetDataTableFromDatabase3();
                ExportDataTableToPdf(dtbl, @"C:\Users\JUNCTION\Documents\OneNote Notebooks\Jewellery-Store-Management-System-main\Report3.pdf", "Retrieve the projects with at least one evaluation where the marks are below a certain threshold (e.g., 15 out of 30)");
                if (checkBox3.Checked)
                {
                    System.Diagnostics.Process.Start(@"C:\Users\JUNCTION\Documents\OneNote Notebooks\Jewellery-Store-Management-System-main\Report3.pdf");
                    this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Message");
            }
        }

        //.....................................4..........................................

        private DataTable GetDataTableFromDatabase4()
        {
            DataTable dataTable = new DataTable();
            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";
                string query = "SELECT * FROM Student WHERE Id NOT IN (SELECT DISTINCT StudentId FROM GroupStudent);\r\n"; // Replace with your actual SQL query

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(query, connection);
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dataTable);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            return dataTable;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtbl = GetDataTableFromDatabase4();
                ExportDataTableToPdf(dtbl, @"C:\Users\JUNCTION\Documents\OneNote Notebooks\Jewellery-Store-Management-System-main\Report4.pdf", "List students who have not been assigned to any project");
                if (checkBox4.Checked)
                {
                    System.Diagnostics.Process.Start(@"C:\Users\JUNCTION\Documents\OneNote Notebooks\Jewellery-Store-Management-System-main\Report4.pdf");
                    this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Message");
            }
        }
    }
}
