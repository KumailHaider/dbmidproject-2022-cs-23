﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml.Linq;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

namespace Jewellery_Store
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            //this.Close();
            //Form10 f10 = new Form10();
            //f10.Show();
            LoadEvaluationData();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
           
        }

       

        private void label3_Click(object sender, EventArgs e)
        {
            this.Close();
            Form3 f3 = new Form3();
            f3.Show();
        }

        

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
            Form3 f3 = new Form3();
            f3.Show();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Close();
            Form4 frm4 = new Form4();
            frm4.Show();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            this.Close();
            Form4 frm4 = new Form4();
            frm4.Show();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            this.Close();
            Form5 f5 = new Form5();
            f5.Show();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Close();
            Form5 f5 = new Form5();
            f5.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            UpdateButton(sender, e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            InsertEvaluation(sender, e);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Evaluation Table.");
                return;
            }
            // Check if a row is selected in the DataGridView
            else if (dataGridView1.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int id = Convert.ToInt32(selectedRow.Cells["ID"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    // Delete record from Evaluation table
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("DELETE FROM Evaluation WHERE ID = @ID", con);
                        cmd.Parameters.AddWithValue("@ID", id);
                        cmd.ExecuteNonQuery();
                    }

                    MessageBox.Show("Record deleted successfully from the Evaluation table!!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please select a row to delete!");
            }

        }

        private void Form6_Load(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView1_SelectionChanged(sender, e);
        }

        private void InsertEvaluation(object sender, EventArgs e)
        {
            // Check if all fields are filled
            if (name.Text==""||totalmarks.Text==""||totalweightage.Text=="")
            {
                MessageBox.Show("Please enter valid values for all fields!");
                return;
            }

            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();

                    // Insert record into Evaluation table
                    SqlCommand insertCmd = new SqlCommand("INSERT INTO Evaluation (Name, TotalMarks, TotalWeightage) VALUES (@Name, @TotalMarks, @TotalWeightage)", con);
                    insertCmd.Parameters.AddWithValue("@Name", name.Text);
                    insertCmd.Parameters.AddWithValue("@TotalMarks", totalmarks.Text);
                    insertCmd.Parameters.AddWithValue("@TotalWeightage", totalweightage.Text);
                    insertCmd.ExecuteNonQuery();

                    MessageBox.Show("Record inserted successfully into Evaluation table!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
        private void LoadEvaluationData()
        {
            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();

                SqlCommand cmd = new SqlCommand("SELECT Id,Name,TotalMarks,TotalWeightage FROM Evaluation", con);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);

                dataGridView1.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void label2_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form2 frm2 = new Form2();
            frm2.Show();
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form2 frm2 = new Form2();
            frm2.Show();
        }
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Evaluation Table!!.");
                return;
            }
            else if (dataGridView1.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int Id = Convert.ToInt32(selectedRow.Cells["Id"].Value);
                

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";



                    // Retrieve data from Evaluation table based on the selected ID
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("SELECT * FROM Evaluation WHERE Id = @Id", con);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            // Fill the textboxes with the relevant project's records
                            name.Text = reader["Name"].ToString();
                            totalmarks.Text = reader["TotalMarks"].ToString();
                            totalweightage.Text = reader["TotalWeightage"].ToString();

                            // Repeat this for other textboxes as needed
                        }
                        else
                        {
                            MessageBox.Show("No records found in Evaluation table for the selected ID.");
                            return;
                        }

                        reader.Close();
                    }
                        
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }

        }
        private void UpdateButton(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Evaluation Table.");
                return;
            }
            else if (dataGridView1.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int evaluationId = Convert.ToInt32(selectedRow.Cells["ID"].Value); // Assuming the ID column is named ID

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();

                        // Update record in Evaluation table
                        SqlCommand updateEvaluationCmd = new SqlCommand("UPDATE Evaluation SET Name=@Name, TotalMarks=@TotalMarks, TotalWeightage=@TotalWeightage WHERE Id = @Id", con);
                        updateEvaluationCmd.Parameters.AddWithValue("@Name", name.Text); // Assuming nameTextBox is the TextBox for Name
                        updateEvaluationCmd.Parameters.AddWithValue("@TotalMarks", Convert.ToInt32(totalmarks.Text)); // Assuming totalMarksTextBox is the TextBox for TotalMarks
                        updateEvaluationCmd.Parameters.AddWithValue("@TotalWeightage", Convert.ToInt32(totalweightage.Text)); // Assuming totalWeightageTextBox is the TextBox for TotalWeightage
                        updateEvaluationCmd.Parameters.AddWithValue("@Id", evaluationId);
                        updateEvaluationCmd.ExecuteNonQuery();

                        MessageBox.Show("Record updated successfully in Evaluation table!!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please select a row to update!");
            }
        }

        //..........................Group Evaluation......................................

        private void InsertGroupEvaluation()
        {
            if (dateTimePicker1.Text == "" || evobtain.Text=="")
            {
                MessageBox.Show("Please enter values for all fields.");
                return;
            }

            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                // Get the GroupId based on the created date
                DateTime createdOn = dateTimePicker2.Value.Date;
                int groupId = GetGroupId(createdOn, connectionString);
                if (groupId == -1)
                {
                    MessageBox.Show("No group found for the specified creation date.");
                    return;
                }

                // Get the EvaluationId based on the evaluation name
                string evaluationName = evname.Text; // Assuming evaluationNameTextBox is the TextBox for evaluation name
                int evaluationId = GetEvaluationId(evaluationName, connectionString);
                if (evaluationId == -1)
                {
                    MessageBox.Show("No evaluation found for the specified name.");
                    return;
                }

                // Get the obtained marks from the user input
                int obtainedMarks = Convert.ToInt32(evobtain.Text); // Assuming obtainedMarksTextBox is the TextBox for obtained marks

                // Get the evaluation date from the DateTimePicker
                DateTime evaluationDate = dateTimePicker1.Value; // Assuming dateTimePicker1 is the DateTimePicker for evaluation date

                // Insert record into GroupEvaluation table
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();

                    SqlCommand insertCmd = new SqlCommand("INSERT INTO GroupEvaluation (GroupId, EvaluationId, ObtainedMarks, EvaluationDate) VALUES (@GroupId, @EvaluationId, @ObtainedMarks, @EvaluationDate)", con);
                    insertCmd.Parameters.AddWithValue("@GroupId", groupId);
                    insertCmd.Parameters.AddWithValue("@EvaluationId", evaluationId);
                    insertCmd.Parameters.AddWithValue("@ObtainedMarks", obtainedMarks);
                    insertCmd.Parameters.AddWithValue("@EvaluationDate", evaluationDate);
                    insertCmd.ExecuteNonQuery();

                    MessageBox.Show("Record inserted successfully into GroupEvaluation table!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        // Method to get the GroupId based on the created date
        private int GetGroupId(DateTime createdOn, string connectionString)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();

                    SqlCommand cmd = new SqlCommand("SELECT Id FROM [Group] WHERE Created_On = @Created_On", con);
                    cmd.Parameters.AddWithValue("@Created_On", createdOn);

                    object groupIdResult = cmd.ExecuteScalar();
                    return groupIdResult != null ? Convert.ToInt32(groupIdResult) : -1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return -1;
            }
        }

        // Method to get the EvaluationId based on the evaluation name
        private int GetEvaluationId(string evaluationName, string connectionString)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();

                    SqlCommand cmd = new SqlCommand("SELECT Id FROM Evaluation WHERE Name = @Name", con);
                    cmd.Parameters.AddWithValue("@Name", evaluationName);

                    object evaluationIdResult = cmd.ExecuteScalar();
                    return evaluationIdResult != null ? Convert.ToInt32(evaluationIdResult) : -1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                return -1;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            InsertGroupEvaluation();
        }
        private void LoadGroupEvaluationData()
        {
            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();

                SqlCommand cmd = new SqlCommand("SELECT GroupId,EvaluationId,ObtainedMarks,EvaluationDate FROM GroupEvaluation", con);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);

                dataGridView2.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            LoadGroupEvaluationData();
        }
        private void dataGridView2_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Group Evaluation Table!!.");
                return;
            }
            else if (dataGridView2.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView2.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int groupId = Convert.ToInt32(selectedRow.Cells["GroupId"].Value);
                int evaluationId = Convert.ToInt32(selectedRow.Cells["EvaluationId"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";



                    // Retrieve data from Evaluation table based on the selected ID
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("SELECT * FROM Evaluation WHERE Id = @Id", con);
                        cmd.Parameters.AddWithValue("@Id", evaluationId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            // Fill the textboxes with the relevant project's records
                            evname.Text = reader["Name"].ToString();

                            // Repeat this for other textboxes as needed
                        }
                        else
                        {
                            MessageBox.Show("No records found in Evaluation table for the selected ID.");
                            return;
                        }

                        reader.Close();
                    }

                    // Retrieve data from Group table based on the selected ID
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("SELECT * FROM [Group] WHERE Id = @Id", con);
                        cmd.Parameters.AddWithValue("@Id", groupId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            // Fill the textboxes with the relevant group's records
                            dateTimePicker2.Text = reader["Created_On"].ToString();
                            //comboBox1.Text = reader["AdvisorRole"].ToString();

                        }
                        else
                        {
                            MessageBox.Show("No records found in Group table for the selected ID.");
                            return;
                        }

                        reader.Close();
                    }
                    // Retrieve data from GroupEvaluation table based on the selected ID
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("SELECT * FROM GroupEvaluation WHERE GroupId = @GroupId", con);
                        cmd.Parameters.AddWithValue("@GroupId", groupId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            // Fill the textboxes with the relevant group's records
                            dateTimePicker1.Text = reader["EvaluationDate"].ToString();
                            evobtain.Text = reader["ObtainedMarks"].ToString();

                        }
                        else
                        {
                            MessageBox.Show("No records found in Group Evaluation table for the selected ID.");
                            return;
                        }

                        reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }

        }

        private void button6_Click(object sender, EventArgs e)
        {
            dataGridView2_SelectionChanged(sender, e);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Group Evaluation Table!!.");
                return;
            }
            // Check if a row is selected in the DataGridView
            else if (dataGridView2.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView2.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int evaluationId = Convert.ToInt32(selectedRow.Cells["EvaluationId"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    // Delete record from GroupStudent table
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("DELETE FROM GroupEvaluation WHERE EvaluationId = @EvaluationId", con);
                        cmd.Parameters.AddWithValue("@EvaluationId", evaluationId);
                        cmd.ExecuteNonQuery();
                    }

                    MessageBox.Show("Record deleted successfully from the Group Evaluation table!!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please select a row to delete!");
            }
        }
        private void UpdateGroupEvaluationButton(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Group Evaluation Table.");
                return;
            }
            else if (dataGridView2.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView2.SelectedRows[0];

                // Get the value of the Evaluation ID and Group ID columns from the selected row
                int originalEvaluationId = Convert.ToInt32(selectedRow.Cells["EvaluationId"].Value);
                int originalGroupId = Convert.ToInt32(selectedRow.Cells["GroupId"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();

                        // Update record in Group Project table
                        SqlCommand updateGroupProjectCmd = new SqlCommand("UPDATE GroupEvaluation SET EvaluationId=@NewEvaluationId, GroupId=@NewGroupId,ObtainedMarks=@ObtainedMarks, EvaluationDate=@EvaluationDate WHERE EvaluationId = @OriginalEvaluationId AND GroupId = @OriginalGroupId", con);

                        // Assuming dateTimePicker3 is the control for selecting the date
                        updateGroupProjectCmd.Parameters.AddWithValue("@NewEvaluationId", GetNewEvaluationId());
                        updateGroupProjectCmd.Parameters.AddWithValue("@NewGroupId", GetNewGroupId());
                        updateGroupProjectCmd.Parameters.AddWithValue("@EvaluationDate", dateTimePicker1.Value);
                        updateGroupProjectCmd.Parameters.AddWithValue("@ObtainedMarks", Convert.ToInt32(evobtain.Text));
                        updateGroupProjectCmd.Parameters.AddWithValue("@OriginalEvaluationId", originalEvaluationId);
                        updateGroupProjectCmd.Parameters.AddWithValue("@OriginalGroupId", originalGroupId);

                        updateGroupProjectCmd.ExecuteNonQuery();

                        MessageBox.Show("Record updated successfully in Group Evaluation table!!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please select a row to update!");
            }
        }

        private int GetNewGroupId()
        {
            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();

                    // Retrieve the Group ID based on the creation date
                    DateTime creationDate = dateTimePicker2.Value;
                    SqlCommand cmd = new SqlCommand("SELECT Id FROM [Group] WHERE Created_On = @Created_On", con);
                    cmd.Parameters.AddWithValue("@Created_On", creationDate);

                    object groupIdResult = cmd.ExecuteScalar();
                    if (groupIdResult != null)
                    {
                        return Convert.ToInt32(groupIdResult);
                    }
                    else
                    {
                        MessageBox.Show("No group found for the specified creation date.");
                        // You may want to handle this case based on your application logic
                        return -1; // or throw an exception, depending on your requirement
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                // You may want to handle this exception based on your application logic
                return -1; // or throw an exception, depending on your requirement
            }
        }

        // Method to get a new Project ID based on the project title
        private int GetNewEvaluationId()
        {
            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();

                    // Retrieve the Project ID based on the title
                    string eName = evname.Text; // Assuming ptitle is the TextBox for project title
                    SqlCommand cmd = new SqlCommand("SELECT Id FROM Evaluation WHERE Name = @Name", con);
                    cmd.Parameters.AddWithValue("@Name", eName);

                    object projectIdResult = cmd.ExecuteScalar();
                    if (projectIdResult != null)
                    {
                        return Convert.ToInt32(projectIdResult);
                    }
                    else
                    {
                        MessageBox.Show("No project found for the specified name.");
                        // You may want to handle this case based on your application logic
                        return -1; // or throw an exception, depending on your requirement
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                // You may want to handle this exception based on your application logic
                return -1; // or throw an exception, depending on your requirement
            }
        }




        private void button8_Click(object sender, EventArgs e)
        {
            UpdateGroupEvaluationButton(sender, e);
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label3_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form3 f3 = new Form3();
            f3.Show();
        }

        private void pictureBox2_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form3 f3 = new Form3();
            f3.Show();
        }

        private void label5_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form4 f4 = new Form4();
            f4.Show();
        }

        private void pictureBox5_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form4 f4 = new Form4();
            f4.Show();
        }

        private void label4_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form5 f5 = new Form5();
            f5.Show();
        }

        private void pictureBox3_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form5 f5 = new Form5();
            f5.Show();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            this.Close();
            Form9 f9 = new Form9();
            f9.Show();
        }

        private void pictureBox6_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form9 f9 = new Form9();
            f9.Show();
        }

        private void label6_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form10 f10 = new Form10();
            f10.Show();
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            this.Close();
            Form10 f10 = new Form10();
            f10.Show();
        }

        private void label16_Click(object sender, EventArgs e)
        {
            this.Close();
            Form6 f6 = new Form6();
            f6.Show();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            this.Close();
            Form6 f6 = new Form6();
            f6.Show();
        }
    }
}
