﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using System.Net.NetworkInformation;

namespace Jewellery_Store
{
    public partial class Form3 : Form
    {
        


        
        public Form3()
        {
            InitializeComponent();

        }

        private void Form3_Load(object sender, EventArgs e)
        {
            //populate();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 frm2 = new Form2();
            frm2.Show();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Close();
            Form5 f5 = new Form5();
            f5.Show();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {
            this.Close();
            Form6 f6 = new Form6();
            f6.Show();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Close();
            Form6 f6 = new Form6();
            f6.Show();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            this.Close();
            Form5 f5 = new Form5();
            f5.Show();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form4 frm4 = new Form4();
            frm4.Show();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form4 frm4 = new Form4();
            frm4.Show();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 frm2 = new Form2();
            frm2.Show();
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                if (regno.Text == "" || eid.Text == "" || ename.Text == "" || ecnic.Text == "" || eph.Text == "" || comboBox1.Text == "")
                {
                    MessageBox.Show("Please fill all the fields");
                }
                else
                {
                    try
                    {
                        // Convert the date to the expected SQL format
                        string dateOfBirth = dateTimePicker1.Value.ToString("yyyy-MM-dd");

                        // Check if a person with the same details already exists
                        
                        SqlCommand checkCmd = new SqlCommand("SELECT Id FROM Person WHERE FirstName = @FirstName AND LastName = @LastName AND Contact = @Contact AND Email = @Email AND DateOfBirth = @DateOfBirth AND Gender = @Gender", con);
                        checkCmd.Parameters.AddWithValue("@FirstName", eid.Text);
                        checkCmd.Parameters.AddWithValue("@LastName", ename.Text);
                        checkCmd.Parameters.AddWithValue("@Contact", ecnic.Text);
                        checkCmd.Parameters.AddWithValue("@Email", eph.Text);
                        checkCmd.Parameters.AddWithValue("@DateOfBirth", dateOfBirth);
                        if (comboBox1.Text == "Male")
                        {
                            checkCmd.Parameters.AddWithValue("@Gender", 1);
                        }
                        else
                        {
                            checkCmd.Parameters.AddWithValue("@Gender", 2);
                        }

                        object personIdResult = checkCmd.ExecuteScalar();

                        if (personIdResult != null)
                        {
                            int personId = Convert.ToInt32(personIdResult);

                            // Insert record into Student table using the existing person ID
                            SqlCommand insertStudentCmd = new SqlCommand("INSERT INTO Student (Id, RegistrationNo) VALUES (@Id, @RegistrationNo)", con);
                            insertStudentCmd.Parameters.AddWithValue("@Id", personId);
                            insertStudentCmd.Parameters.AddWithValue("@RegistrationNo", regno.Text);
                            insertStudentCmd.ExecuteNonQuery();

                            MessageBox.Show("Record Added Successfully using existing person details!");
                        }
                        else
                        {
                            // If person with the same details doesn't exist, insert a new person
                            SqlCommand insertPersonCmd = new SqlCommand("INSERT INTO Person (FirstName, LastName, Contact, Email, DateOfBirth, Gender) VALUES (@FirstName, @LastName, @Contact, @Email, @DateOfBirth, @Gender); SELECT SCOPE_IDENTITY()", con);

                            insertPersonCmd.Parameters.AddWithValue("@FirstName", eid.Text);
                            insertPersonCmd.Parameters.AddWithValue("@LastName", ename.Text);
                            insertPersonCmd.Parameters.AddWithValue("@Contact", ecnic.Text);
                            insertPersonCmd.Parameters.AddWithValue("@Email", eph.Text);
                            insertPersonCmd.Parameters.AddWithValue("@DateOfBirth",dateOfBirth);
                            if (comboBox1.Text == "Male")
                            {
                                insertPersonCmd.Parameters.AddWithValue("@Gender", 1);
                            }
                            else
                            {
                                insertPersonCmd.Parameters.AddWithValue("@Gender", 2);
                            }

                            // Execute the command to insert the person and retrieve the generated ID
                            int newPersonId = Convert.ToInt32(insertPersonCmd.ExecuteScalar());

                            // Insert record into Student table using the newly inserted person ID
                            SqlCommand insertStudentCmd = new SqlCommand("INSERT INTO Student (Id, RegistrationNo) VALUES (@Id, @RegistrationNo)", con);
                            insertStudentCmd.Parameters.AddWithValue("@Id", newPersonId);
                            insertStudentCmd.Parameters.AddWithValue("@RegistrationNo", regno.Text);
                            insertStudentCmd.ExecuteNonQuery();

                            MessageBox.Show("Record Added Successfully using new person details!");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: " + ex.Message);
                    }
                }
            }

        }
        private void LoadStudentData()
        {
            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();

                SqlCommand cmd = new SqlCommand("SELECT Id, RegistrationNo FROM Student", con);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);

                dataGridView1.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            //populate();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            UpdateButton_Click(sender, e);
        }

        private void EmpDataTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            
        }

        private void button4_Click(object sender, EventArgs e)
        {

            DeleteRecord();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
            Form11 f11 = new Form11();
            f11.Show();
            

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            LoadStudentData();
        }

        private void button7_Click(object sender, EventArgs e)
        {


        }
        
        private void DeleteRecord()
        {
            // Check if a row is selected in the DataGridView
            if (dataGridView1.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int id = Convert.ToInt32(selectedRow.Cells["ID"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    // Delete record from Student table
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("DELETE FROM Student WHERE Id = @Id", con);
                        cmd.Parameters.AddWithValue("@Id", id);
                        cmd.ExecuteNonQuery();
                    }

                    MessageBox.Show("Record deleted successfully from the Student table!!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please select a row to delete!");
            }

        }

        
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int id = Convert.ToInt32(selectedRow.Cells["ID"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    // Retrieve data from Person table based on the selected ID
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("SELECT * FROM Person WHERE Id = @Id", con);
                        cmd.Parameters.AddWithValue("@Id", id);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            // Fill the textboxes with the relevant person's records
                            eid.Text = reader["FirstName"].ToString();
                            ename.Text = reader["LastName"].ToString();
                            ecnic.Text = reader["Contact"].ToString();
                            eph.Text = reader["Email"].ToString();
                            comboBox1.Text = reader["Gender"].ToString();

                            // Repeat this for other textboxes as needed
                        }
                        else
                        {
                            MessageBox.Show("No records found in Person table for the selected ID.");
                            return;
                        }

                        reader.Close();
                    }
                    // Retrieve data from Student table based on the selected ID
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE Id = @Id", con);
                        cmd.Parameters.AddWithValue("@Id", id);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            // Fill the textboxes with the relevant student's records
                            regno.Text = reader["RegistrationNo"].ToString();
                            
                        }
                        else
                        {
                            MessageBox.Show("No records found in Student table for the selected ID.");
                            return;
                        }

                        reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int id = Convert.ToInt32(selectedRow.Cells["ID"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    // Update records in both tables
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();

                        // Update record in Student table
                        SqlCommand updateStudentCmd = new SqlCommand("UPDATE Student SET RegistrationNo=@RegistrationNo WHERE Id = @Id", con);
                        updateStudentCmd.Parameters.AddWithValue("@RegistrationNo", regno.Text);
                        updateStudentCmd.Parameters.AddWithValue("@Id", id);
                        updateStudentCmd.ExecuteNonQuery();

                        // Update record in Person table
                        SqlCommand updatePersonCmd = new SqlCommand("UPDATE Person SET FirstName = @FirstName, LastName = @LastName, Contact=@Contact, Email=@Email WHERE Id = @Id", con);
                        updatePersonCmd.Parameters.AddWithValue("@FirstName", eid.Text);
                        updatePersonCmd.Parameters.AddWithValue("@LastName", ename.Text);
                        updatePersonCmd.Parameters.AddWithValue("@Contact", ecnic.Text);
                        updatePersonCmd.Parameters.AddWithValue("@Email", eph.Text);
                        updatePersonCmd.Parameters.AddWithValue("@Id", id);
                        updatePersonCmd.ExecuteNonQuery();

                        MessageBox.Show("Record updated successfully in both Student and Person tables!!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please select a row to update!");
            }
        }

        private void regno_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            dataGridView1_SelectionChanged(sender, e);
        }

        private void label5_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            Form4 frm4 = new Form4();
            frm4.Show();
        }

        private void label4_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form5 f5 = new Form5();
            f5.Show();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            this.Close();
            Form9 f9 = new Form9();
            f9.Show();
        }

        private void pictureBox6_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form9 f9 = new Form9();
            f9.Show();
        }

        private void label6_Click_1(object sender, EventArgs e)
        {
            this.Close();
            Form10 f10 = new Form10();
            f10.Show();
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            this.Close();
            Form10 f10 = new Form10();
            f10.Show();
        }

        private void label16_Click(object sender, EventArgs e)
        {
            this.Close();
            Form6 f6 = new Form6();
            f6.Show();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            this.Close();
            Form6 f6 = new Form6();
            f6.Show();
        }
        
    }
}
