﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using System.Security.Cryptography;
using System.Xml.Linq;

namespace Jewellery_Store
{
    
    public partial class Form8 : Form
    {
        public void populate()
        {
            
        }
        public Form8()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            Form4 f4 = new Form4();
            f4.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void CusDataT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form8_Load(object sender, EventArgs e)
        {
            //populate();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();

                if (pid.Text == "" ||comboBox1.Text == "" || comboBox2.Text == "" || dateTimePicker1.Text == ""||cid.Text=="")
                {
                    MessageBox.Show("Please fill all the fields");
                }
                else
                {
                    try
                    {
                        // Convert the date to the expected SQL format
                        string assignmentDate = dateTimePicker1.Value.ToString("yyyy-MM-dd");

                        // Check if an advisor with the same details already exists
                        SqlCommand checkCmd = new SqlCommand("SELECT Id FROM Advisor WHERE Designation=@Designation AND Salary=@Salary" , con);
                        checkCmd.Parameters.AddWithValue("@Salary", cid.Text);
                        
                        if (comboBox2.Text == "Professor")
                        {
                            checkCmd.Parameters.AddWithValue("@Designation", 6);
                        }
                        else if (comboBox2.Text == "AssociateProfessor")
                        {
                            checkCmd.Parameters.AddWithValue("@Designation", 7);
                        }
                        else if (comboBox2.Text == "AssisstantProfessor")
                        {
                            checkCmd.Parameters.AddWithValue("@Designation", 8);
                        }
                        else if (comboBox2.Text == "Lecturer")
                        {
                            checkCmd.Parameters.AddWithValue("@Designation", 9);
                        }
                        else
                        {
                            checkCmd.Parameters.AddWithValue("@Designation", 10);
                        }

                        object advisorIdResult = checkCmd.ExecuteScalar();
                        int advisorId;

                        
                        
                        advisorId = Convert.ToInt32(advisorIdResult);
                        

                        // Get the Project ID
                        SqlCommand getProjectIdCmd = new SqlCommand("SELECT Id FROM Project WHERE Title = @Title", con);
                        getProjectIdCmd.Parameters.AddWithValue("@Title", pid.Text);
                        //getProjectIdCmd.Parameters.AddWithValue("@Description", pdes.Text);

                        object projectIdResult = getProjectIdCmd.ExecuteScalar();
                        int projectId;

                        if (projectIdResult != null)
                        {
                            projectId = Convert.ToInt32(projectIdResult);
                        }
                        else
                        {
                            // If project with the same details doesn't exist, show error message
                            MessageBox.Show("No project found with the provided title and description.");
                            return;
                        }

                        // Insert record into ProjectAdvisor table
                        SqlCommand insertProjectAdvisorCmd = new SqlCommand("INSERT INTO ProjectAdvisor (AdvisorId, ProjectId, AdvisorRole, AssignmentDate) VALUES (@AdvisorId, @ProjectId, @AdvisorRole, @AssignmentDate)", con);
                        insertProjectAdvisorCmd.Parameters.AddWithValue("@AdvisorId", advisorId);
                        insertProjectAdvisorCmd.Parameters.AddWithValue("@ProjectId", projectId);
                        insertProjectAdvisorCmd.Parameters.AddWithValue("@AdvisorRole", GetAdvisorRoleValue(comboBox1.Text));
                        insertProjectAdvisorCmd.Parameters.AddWithValue("@AssignmentDate", assignmentDate);
                        insertProjectAdvisorCmd.ExecuteNonQuery();

                        MessageBox.Show("Record Added Successfully!");

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: " + ex.Message);
                    }
                }
            }


        }
        // Helper function to map advisor role string to corresponding int value in the database
        private int GetAdvisorRoleValue(string role)
        {
            switch (role)
            {
                case "MainAdvisor":
                    return 11;
                case "Co-Advisor":
                    return 12;
                
                default:
                    return 14;
            }
        }
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Project Advisor Table!!.");
                return;
            }
            else if (dataGridView1.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int advisorId = Convert.ToInt32(selectedRow.Cells["AdvisorId"].Value);
                int projectId = Convert.ToInt32(selectedRow.Cells["ProjectId"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                   

                    // Retrieve data from Project table based on the selected ID
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("SELECT * FROM Project WHERE Id = @ProjectId", con);
                        cmd.Parameters.AddWithValue("@ProjectId", projectId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            // Fill the textboxes with the relevant project's records
                            pid.Text = reader["Title"].ToString();

                            // Repeat this for other textboxes as needed
                        }
                        else
                        {
                            MessageBox.Show("No records found in Project table for the selected ID.");
                            return;
                        }

                        reader.Close();
                    }

                    // Retrieve data from ProjectAdvisor table based on the selected ID
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("SELECT * FROM ProjectAdvisor WHERE AdvisorId = @AdvisorId", con);
                        cmd.Parameters.AddWithValue("@AdvisorId", advisorId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            // Fill the textboxes with the relevant Project advisor's records
                            dateTimePicker1.Text = reader["AssignmentDate"].ToString();
                            comboBox1.Text = reader["AdvisorRole"].ToString();

                        }
                        else
                        {
                            MessageBox.Show("No records found in Project Advisor table for the selected ID.");
                            return;
                        }

                        reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }

        }
        private void LoadProjectAdvisorData()
        {
            try
            {
                string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();

                SqlCommand cmd = new SqlCommand("SELECT AdvisorId, ProjectId,AdvisorRole,AssignmentDate FROM ProjectAdvisor", con);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);

                dataGridView1.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            LoadProjectAdvisorData();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            dataGridView1_SelectionChanged(sender, e);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Project Advisor Table!!.");
                return;
            }
            // Check if a row is selected in the DataGridView
            else if (dataGridView1.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];

                // Get the value of the ID column from the selected row
                int advisorId = Convert.ToInt32(selectedRow.Cells["AdvisorId"].Value);

                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    // Delete record from Student table
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("DELETE FROM ProjectAdvisor WHERE AdvisorId = @AdvisorId", con);
                        cmd.Parameters.AddWithValue("@AdvisorId", advisorId);
                        cmd.ExecuteNonQuery();
                    }

                    MessageBox.Show("Record deleted successfully from the Project Advisor table!!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please select a row to delete!");
            }
        }
        private void UpdateButton_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select a row from the Project Advisor Table!!");
                return;
            }
            else if (dataGridView1.SelectedRows.Count > 0)
            {
                // Get the selected row
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];
                int advisorId = Convert.ToInt32(selectedRow.Cells["AdvisorId"].Value);
                try
                {
                    string connectionString = @"Data Source=(local);Initial Catalog=ProjectAScripts;Integrated Security=True;";

                    // Get the project ID based on the project title entered by the user
                    int projectId;
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();

                        SqlCommand getProjectIdCmd = new SqlCommand("SELECT Id FROM Project WHERE Title = @Title", con);
                        getProjectIdCmd.Parameters.AddWithValue("@Title", pid.Text);
                        object projectIdResult = getProjectIdCmd.ExecuteScalar();

                        if (projectIdResult == null)
                        {
                            MessageBox.Show("Project does not exist.");
                            return;
                        }
                        else
                        {
                            projectId = Convert.ToInt32(projectIdResult);
                        }
                    }

                    // If the project exists, proceed with updating the record in ProjectAdvisor table
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();

                        // Update record in ProjectAdvisor table with the retrieved project ID
                        SqlCommand updateProjectAdvisorCmd = new SqlCommand("UPDATE ProjectAdvisor SET ProjectID=@ProjectId WHERE AdvisorId = @AdvisorId", con);
                        updateProjectAdvisorCmd.Parameters.AddWithValue("@ProjectId", projectId);
                        updateProjectAdvisorCmd.Parameters.AddWithValue("@AdvisorId", advisorId);
                        updateProjectAdvisorCmd.ExecuteNonQuery();

                        // Update record in Project Advisor table
                        DateTime assignmentDate = dateTimePicker1.Value;
                        SqlCommand updateAdvisorCmd = new SqlCommand("UPDATE ProjectAdvisor SET AdvisorRole=@AdvisorRole, AssignmentDate=@AssignmentDate WHERE AdvisorId = @AdvisorId", con);
                        updateAdvisorCmd.Parameters.AddWithValue("@AdvisorRole", GetDesignationId(comboBox1.Text));
                        updateAdvisorCmd.Parameters.AddWithValue("@AssignmentDate", assignmentDate);
                        updateAdvisorCmd.Parameters.AddWithValue("@AdvisorId", advisorId);
                        updateAdvisorCmd.ExecuteNonQuery();

                        MessageBox.Show("Record updated successfully in Project Advisor table!!");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }



        }
        private int GetDesignationId(string designation)
        {
            // Define a dictionary to map designation strings to their corresponding integer values
            Dictionary<string, int> designationMappings = new Dictionary<string, int>
            {
                { "MainAdvisor", 11 },
                { "Co-Advisor", 12 },
                {"IndustryAdvisor",14 },
                // Add more mappings as needed
            };

            // Get the integer designation based on the selected string value
            return designationMappings.ContainsKey(designation) ? designationMappings[designation] : 10;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            UpdateButton_Click(sender, e);
        }
        















    }
}
